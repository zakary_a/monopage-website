import { ManagerService } from './../manager.service';
import { Component, OnInit} from '@angular/core';

let tracker =0;
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})

export class HeaderComponent implements OnInit {
  constructor(public service: ManagerService) {}
  

  ngOnInit(): void {
  }
  

  popup():void {
    
      var popup = document.getElementById("popup");
              
        if(popup.classList.contains("hide")==true){
          popup.classList.remove("hide");
          popup.classList.add("show");
        }
        else{
          popup.classList.remove("show");
          popup.classList.add("hide");
        }
  }
}
