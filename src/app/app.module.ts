import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
 

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ArticlesComponent } from './articles/articles.component';
import { AchatComponent } from './achat/achat.component';
import { CommandesComponent } from './commandes/commandes.component';
import { GerantComponent } from './gerant/gerant.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { BannerComponent } from './banner/banner.component';
import { MessageComponent } from './message/message.component';
import { LocationComponent } from './location/location.component';
import { MenuComponent } from './menu/menu.component';
import { ViewComponent } from './view/view.component';
import { PayerComponent } from './payer/payer.component';


@NgModule({
  declarations: [
    AppComponent,
    ArticlesComponent,
    AchatComponent,
    CommandesComponent,
    GerantComponent,
    HeaderComponent,
    FooterComponent,
    BannerComponent,
    MessageComponent,
    LocationComponent,
    MenuComponent,
    ViewComponent,
    PayerComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
