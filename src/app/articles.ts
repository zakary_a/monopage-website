export interface articles {
    id: string;
    nom: string;
    marque: string;
    prix: string;
    description: string;
    image: string;
}