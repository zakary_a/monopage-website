import { ProductService } from './../product.service';
import { articles } from './../articles';
import { Component, OnInit, PipeTransform, Pipe } from '@angular/core';
import bd from '../../assets/json/bd.json';

@Component({
  selector: 'app-articles',
  templateUrl: './articles.component.html',
  styleUrls: ['./articles.component.css'],
})

export class ArticlesComponent implements OnInit {
  
  constructor(public service: ProductService){}
  ngOnInit(){ 
    this.getArticles();
  }
  public articleList: articles[]=[];
  getArticles(): void {
    for(let x=0;x<bd.nombre;x++){
      this.articleList.push(bd.produits[x]);
    }
  }
  getsize(): number{
    return(bd.nombre);
  }
 
  printPicture(number): string {
    return(this.articleList[number].image);
  }
  printName(number): string {
    return(this.articleList[number].nom);
  }
  printBrand(number): string {
    return(this.articleList[number].marque);
  }
  printDescription(number): string {
    return(this.articleList[number].description);
  }
  printPrice(number): number {
    return parseInt(this.articleList[number].prix);
  }
  printID(number): string {
    return(this.articleList[number].id);
  }
}
