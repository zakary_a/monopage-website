import { ManagerService } from './../manager.service';
import { ProductService } from './../product.service';

import { articles } from './../articles';
import { Component, OnInit} from '@angular/core';
import bd from '../../assets/json/bd.json';



@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit {

  constructor(public service: ProductService, public service2: ManagerService) { }
  price=0;
  
  getPrice():void{
    this.price=this.service.sum;
  }
  ngOnInit(): void {
    this.getArticles()
  }
 
  articleList: articles[]=[];
  getArticles(): void {
    for(let x=0;x<bd.nombre;x++){
      this.articleList.push(bd.produits[x]);
    }
  }
  getsize(): number{
    return(bd.nombre);
  }
 
  printPicture(number): string {
    return(this.articleList[number].image);
  }
  printName(number): string {
    return(this.articleList[number].nom);
  }
  printBrand(number): string {
    return(this.articleList[number].marque);
  }
  printDescription(number): string {
    return(this.articleList[number].description);
  }
  printPrice(number): number {
    return parseInt(this.articleList[number].prix);
  }
  printID(number): string {
    return(this.articleList[number].id);
  }

}
