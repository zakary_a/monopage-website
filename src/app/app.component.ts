import { articles } from './articles';
import { ManagerService } from './manager.service';
import { Component } from '@angular/core';
import { QueryBindingType } from '@angular/compiler/src/core';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
constructor(public service: ManagerService) {}
 title="Qi";
}
