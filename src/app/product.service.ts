import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  number=[0,0,0,0,0];
  public sum:number=0;
  

  add(number:number,price:number):void{
    this.number[number]++;
    this.sum=this.sum+price;
    }
  less(number:number,price:number):void{
    if(this.number[number]>0){
    this.number[number]--;
    this.sum=this.sum-price;
    }
  }


  constructor() { }
}
