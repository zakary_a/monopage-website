import { Component, OnInit } from '@angular/core';
import { ManagerService } from './../manager.service';

@Component({
  selector: 'app-gerant',
  templateUrl: './gerant.component.html',
  styleUrls: ['./gerant.component.css']
})
export class GerantComponent implements OnInit {

  constructor(private service: ManagerService) { }
  setContact():void {
    this.service.setLocation();
  }
  ngOnInit(): void {
  }

}
